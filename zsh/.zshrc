# If you come from bash you might have to change your $PATH.
export PATH=$PATH:/usr/games:/usr/local/games

# Path to your oh-my-zsh installation.
  export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="amuse"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="yyyy-mm-dd"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  autopep8
  colored-man-pages
  command-not-found
  common-aliases
  compleat
  extract
  git
  gitfast
  pep8
  pylint
  python
  screen
)

# Disable check of insecure directories to be able to perform sudo -Es
ZSH_DISABLE_COMPFIX=true

source $ZSH/oh-my-zsh.sh

# User configuration

# The user for which ZSH theme should not display name
DEFAULT_USER="moamoak"

# Sett the LC tot french if available
if locale -a | grep -q "fr_FR.utf8"; then
    LC_COMMON="fr_FR.utf8"
else
    LC_COMMON="en_US.utf8"
fi
LC_CTYPE=$LC_COMMON
LC_NUMERIC=$LC_COMMON
LC_TIME=$LC_COMMON
LC_COLLATE=$LC_COMMON
LC_MONETARY=$LC_COMMON
LC_MESSAGES=$LC_COMMON
LC_PAPER=$LC_COMMON
LC_NAME=$LC_COMMON
LC_ADDRESS=$LC_COMMON
LC_TELEPHONE=$LC_COMMON
LC_MEASUREMENT=$LC_COMMON
LC_IDENTIFICATION=$LC_COMMON
LC_ALL=$LC_COMMON
LANG=$LC_COMMON

export MANPATH="/usr/local/man:$MANPATH"

export EDITOR='vim'


# ALIASES
# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# Alias for self-updating commands
live () {
    typeset REFRESH_TIME="1s"
    while true; do
        clear
        eval $@
        sleep "$REFRESH_TIME"
    done;
}

# Alias for git
alias gloganop="git --no-pager log --oneline --graph --all --decorate --max-count=$(($(tput lines)-15))"
gsns () {
    git show --abbrev-commit --name-status $@ \
        | sed -r "s/^(A\t.*)/$fg[green]\1$reset_color/" \
        | sed -r "s/^(C\t.*)/$fg_bold[blue]\1$reset_color/" \
        | sed -r "s/^(D\t.*)/$fg[red]\1$reset_color/" \
        | sed -r "s/^(M\t.*)/$fg_bold[yellow]\1$reset_color/" \
        | sed -r "s/^(R[0-9]*\t.*)/$fg[blue]\1$reset_color/" \
        | sed -r "s/^(T\t.*)/$fg[blue]\1$reset_color/" \
        | sed -r "s/^(U\t.*)/$fg_bold[white]\1$reset_color/" \
        | sed -r "s/^(X\t.*)/$fg[gray]\1$reset_color/" \
        | sed -r "s/^(B\t.*)/$fg[red]\1$reset_color/" \
        | sed -r "s/^(M\t.*)/$fg_bold[yellow]\1$reset_color/"
}
alias glog="git log --oneline --graph --decorate"
alias glogs="git log --oneline --graph --decorate --stat"
alias glogas="git log --oneline --graph --decorate --all --stat"

# Alias for sudo with preserv env
alias sudo="sudo -Es"

# Alias for controlling OpenVPN
ovpn () {
    typeset OVPN_DIR="/etc/openvpn/client/"
    if [ -f "$OVPN_DIR$1/$1.ovpn" ]; then
        sudo openvpn "$OVPN_DIR$1/$1.ovpn"
    else
        echo "$1 is not an available VPN in $OVPN_DIR"
    fi
}
alias ovpnstop="sudo killall -SIGTERM openvpn"
alias ovpnrestart="sudo killall -SIGHUP openvpn"

# Force LXC to change HOME upon attach
alias lxc-attach="lxc-attach --set-var HOME=/root"

# Alias for temporary directories
alias cdtmp="cd $(mktemp -d)"
cptmp () { typeset tmpdir="$(mktemp -d)/"; cp -R $@ "$tmpdir"; echo "$tmpdir" }
mvtmp () { typeset tmpdir="$(mktemp -d)/"; mv $@ "$tmpdir"; echo "$tmpdir" }
bak () {
    while [ "$#" -gt 0 ]; do
        typeset bak_dir="$1.bak"
        typeset counter=0
        while [ -f "$bak_dir" ]; do
            counter=$((counter+1))
            bak_dir="$1.bak_$counter"
        done
        cp -R $1 "$bak_dir"
        shift
    done
}

# Trigger sl for similar mistakes
alias gti="sl"

# GhostScript compression
pdfcompress () {
    typeset out="${1%.*}_compressed.${1##*.}"
    case "$2" in
    "screen")
        ghostscript -q -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/screen -sOUTPUTFILE=$out -f $1
        ;;
    "ebook")
        ghostscript -q -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/ebook -sOUTPUTFILE=$out -f $1
        ;;
    "printer")
        ghostscript -q -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/printer -sOUTPUTFILE=$out -f $1
        ;;
    *)
        ghostscript -q -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dPDFSETTINGS=/prepress -sOUTPUTFILE=$out -f $1
        ;;
    esac
}

# Retries a command on failure.
# $1 - the max number of attempts
# $2... - the command to run
retry() {
    local -r -i max_attempts="$1"; shift
    local -r cmd="$@"
    local -i attempt_num=1

    until $cmd
    do
        if (( attempt_num == max_attempts ))
        then
            echo "Attempt $attempt_num failed and there are no more attempts left!"
            return 1
        else
            echo "Attempt $attempt_num failed! Trying again in $attempt_num seconds..."
            sleep $(( attempt_num++ ))
        fi
    done
}

# Open file manager here
alias f="xdg-open ."

# Deactivate the CTRL-S & CTRL-Q hooks freezing the terminal
stty -ixon -ixoff

# Grep for search in files
alias fgrep="grep -nsT --color=auto"
alias gg="grep -RnsI --exclude-dir='.git' --color=auto"
vgg() {
    vim $(grep -RlsI --exclude-dr='.git' $@ | xargs -I{} echo {})
}
# Ansible variables search functions
alias grepansivars="grep -RnsI --exclude-dir='.git' --color=auto '{{ [^[:space:]]\+ }}'"
ansivars() {
    find $1 -type f | xargs -I{} sed -n -e "s/.*{{ \([^[:space:]]\+\) }}.*/\1/p" {} | sort | uniq
}

# Make a directory and access it
mdcd() {
    mkdir -p $1 && cd $1
}

# Change directory and list files inside
cdl() {
    cd $1 && ls -lFh
}

# Manipulate directory stack
alias d="pushd"
D() {
    pushd $(pwd)
}
cds() {
    cd ~$1
}

# Complete and clean df listing
alias dfh="df -hT -x tmpfs -x squashfs -x devtmpfs"

# Add colors to diff and a more readable command
alias diff="diff --color=auto"
alias diffh="diff --color=auto -yr --suppress-common-lines"

# MOTD
MOTD_SCRIPT="/no/exist"
#MOTD_SCRIPT="$HOME/dotfiles/utils/motd.sh"
if [ -f $MOTD_SCRIPT ]; then
    source $MOTD_SCRIPT
fi
