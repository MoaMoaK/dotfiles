" All system-wide defaults are set in $VIMRUNTIME/debian.vim and sourced by
" the call to :runtime you can find below.  If you wish to change any of those
" settings, you should do it in this file (/etc/vim/vimrc), since debian.vim
" will be overwritten everytime an upgrade of the vim packages is performed.
" It is recommended to make changes after sourcing debian.vim since it alters
" the value of the 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim

" Vim will load $VIMRUNTIME/defaults.vim if the user does not have a vimrc.
" This happens after /etc/vim/vimrc(.local) are loaded, so it will override
" any settings in these files.
" If you don't want that to happen, uncomment the below line to prevent
" defaults.vim from being loaded.
" let g:skip_defaults_vim = 1

" Uncomment the next line to make Vim more Vi-compatible
" NOTE: debian.vim sets 'nocompatible'.  Setting 'compatible' changes numerous
" options, so any other options should be set AFTER setting 'compatible'.
"set compatible

" Vim5 and later versions support syntax highlighting. Uncommenting the next
" line enables syntax highlighting by default.
if has("syntax")
  syntax on
endif

" If using a dark background within the editing area and syntax highlighting
" turn on this option as well
"set background=dark

" Uncomment the following to have Vim jump to the last position when
" reopening a file
"if has("autocmd")
"  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
"endif

" Uncomment the following to have Vim load indentation rules and plugins
" according to the detected filetype.
if has("autocmd")
  filetype plugin indent on
endif

" The following are commented out as they cause vim to behave a lot
" differently from regular Vi. They are highly recommended though.
"set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set incsearch		" Incremental search
"set autowrite		" Automatically save before commands like :next and :make
"set hidden		" Hide buffers when they are abandoned
set mouse=a		" Enable mouse usage (all modes)

" Line number
set number
highlight LineNr term=bold cterm=NONE ctermfg=Grey ctermbg=NONE gui=NONE guifg=Grey guibg=NONE

" Highlight searches
set hlsearch
" Remove HL when double escaping
nnoremap <esc><esc> :silent! nohls<cr>

" Wildmenu options
set wildmenu
" Do not list some files and directories
set wildignore=*.o,*~,*.pyc,*/.git/*,*/.hg/*,*/.svn/*

" Remove bells
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Add parenthesis around selection
vnoremap ( <esc>`>a)<esc>`<i(<esc>lv`>l
vnoremap [ <esc>`>a]<esc>`<i[<esc>lv`>l
vnoremap { <esc>`>a}<esc>`<i{<esc>lv`>l
vnoremap " <esc>`>a"<esc>`<i"<esc>lv`>l
vnoremap ' <esc>`>a'<esc>`<i'<esc>lv`>l
vnoremap <space> <esc>`>a<space><esc>`<i<space><esc>lv`>l

" Saves the files and bypass the permission error
command! W execute 'silent! write !sudo tee % > /dev/null' <bar> edit!

" Tabs
map tt :tabnew<CR>             " Open new tab
map tw :tabclose<CR>           " Close current tab
map tq :tabonly<CR>            " Close all other tab
map t<left> :tabprevious<CR>   " Show previous tab
map t<right> :tabnext<CR>      " Show next tab
" Open new tab with current buffer path
map te :tabedit <C-r>=expand("%:p:h")<CR>

" Open windows with horizontal/vertical split with current buffer path
map <C-w><C-h> :split <C-r>=expand("%:p:h")<CR>
map <C-w><C-v> :vsplit <C-r>=expand("%:p:h")<CR>

" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif

" Set Ansible hosts files as dosini filetype
au BufRead,BufNewFile hosts set filetype=dosini

" Tabs specification, default is to keep as it is (tabs are 8 spaces long)
set tabstop=8 softtabstop=0 shiftwidth=0 noexpandtab
" tab = 4 spaces
au FileType python,go
    \ setlocal tabstop=8 softtabstop=4 shiftwidth=4 expandtab smarttab
" tab = 2 spaces
au FileType html,php,dosini,json,vim,javascript,yaml,css,sh,conf
    \ setlocal tabstop=8 softtabstop=2 shiftwidth=2 expandtab smarttab
" tab = tab of 4
au FileType text
    \ setlocal tabstop=4 softtabstop=0 shiftwidth=0 noexpandtab

" Highlight wrong syntax
au FileType html,php,python,vim,yaml,go
    \ highlight StartingTab guibg=Red ctermbg=1 |
    \ syntax match StartingTab "^\t\+"
au FileType html,php,python,vim,yaml,go
    \ highlight TrailingWhitespace guibg=Orange ctermbg=3 |
    \ syntax match TrailingWhitespace "\s\+$"

" Activate folding on specific file types
function MyFoldText()
  " Get the first line of the fold
  let line = getline(v:foldstart)
  " Remove any '/*', '*/', '{{{', '<!--' or '-->'
  let sub = substitute(line, '/\*\|\*/\|{{{\|<!--\|-->\d\=', '', 'g')
  " Remove any starting and trailing spaces
  let sub2 = substitute(sub, '^[[:space:]]*\|[[:space:]]*$', '', 'g')
  return "[" . sub2 . " ...]"
endfunction

function s:activate_folding()
  set foldcolumn=4
  set foldtext=MyFoldText()
  highlight Folded ctermbg=6 ctermfg=Black
  set foldmethod=indent
  set foldenable
  set foldlevel=99
  noremap zz :call ToggleFolding()<CR>
endfunction

function! ToggleFolding()
  if &foldcolumn
    set foldcolumn=0
  else
    set foldcolumn=4
  endif
endfunction

set foldmethod=manual
au FileType html,php,python,go,json,javascript,css,xml
    \ call s:activate_folding()


" Activate line numbering
function s:activate_line_numbers()
  set number
  highlight CursorLine cterm=NONE ctermfg=yellow
  noremap , :call CycleLineNumbers()<CR>
endfunction

function! CycleLineNumbers()
  if &number
    if &relativenumber
      set number
      set norelativenumber
      set nocursorline
    else
      set nonumber
      set norelativenumber
      set nocursorline
    endif
  else
    set number
    set relativenumber
    set cursorline
  endif
endfunction

au FileType html,php,python,go,json,javascript,css,dosini,yaml,xml,conf
    \ call s:activate_line_numbers()

" Multi-files search
au FileType html,php,python,go,json,javascript,css,dosini,yaml,xml
    \ vnoremap f y:silent! grep -F '<C-R>=escape(@",'/\')<CR>' *<CR>:copen<CR>

" IDE setup
function s:activate_taglist()
    if exists(':TlistToggle')
        let g:Tlist_GainFocus_On_ToggleOpen = 1
        let g:Tlist_Use_Right_Window = 1
        noremap <F3> :TlistToggle<CR>
    endif
endfunction

function s:activate_nerdtree()
    if exists(':NERDTreeToggle')
        noremap <F2> :NERDTreeToggle<CR>
    endif
endfunction

function s:closeAddons()
    " If only addons (Taglist or NERDTree) are left, quit vim or close buffer
    for w in range(1, winnr('$'))
        let l:name = bufname(winbufnr(w))
        if l:name !~? '__Tag_List__\|NERD_Tree_'
            return
        endif
    endfor

    if tabpagenr('$') ==? 1
        execute 'quitall'
    else
    execute 'tabclose'
endfun

" Python IDE setup
au FileType python
    \ call s:activate_taglist() |
    \ call s:activate_nerdtree() |
    \ autocmd WinEnter * silent! call s:closeAddons()
