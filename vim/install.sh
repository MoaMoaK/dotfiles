CUR_DIR=$(dirname $0)
VIM_RC=".vimrc"
VIM_DIR=".vim"

# link configuration
ln -fs "$CUR_DIR/$VIM_RC" "$HOME/$VIM_RC"
[ -d "$CUR_DIR/$VIM_DIR" ] && ln -fs "$CUR_DIR/$VIM_DIR" "$HOME/"
