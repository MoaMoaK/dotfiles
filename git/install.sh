CUR_DIR=$(dirname $0)
GIT_CFG=".gitconfig"
GIT_EXCLUDES=".gitexcludes"

# link configuration
ln -fs "$CUR_DIR/$GIT_CFG" "$HOME/$GIT_CFG"
ln -fs "$CUR_DIR/$GIT_EXCLUDES" "$HOME/$GIT_EXCLUDES"
