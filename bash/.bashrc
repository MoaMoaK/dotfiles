# ~/.bashrc: executed by bash(1) for non-login shells

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    __tput_define() {
        export $1="$(tput $2)"
        export NOSIZE_$1="\[$(tput $2)\]"
    }
else
    __tput_define() {
        export $1=""
        export NOSIZE_$1=""
    }
fi

__color_define() {
    __tput_define FG_$1 "setaf $2"
    __tput_define BG_$1 "setab $2"
}
__tput_define COLOR_RESET sgr0
__tput_define BOLD bold
__color_define BLACK 0
__color_define RED 1
__color_define GREEN 2
__color_define YELLOW 3
__color_define BLUE 4
__color_define MAGENTA 5
__color_define CYAN 6
__color_define WHITE 7

right_align() {
  # Usage: right_align <color sequence> <text>
  c=$1; shift
  echo "$(tput sc; printf "${c}%*s${COLOR_RESET}" $COLUMNS "$@"; tput rc)"
}

unset __color_define
unset __tput_define

test_version() {
    # Compares 2 version number in dot-notation (x.y.z)
    # Exit code is 0 if true, else it is 1
  usage() {
    echo "Usage: $0 <v1> <op> <v2>"
    echo "  where <op> is one of: < lt <= le == = eq >= ge > gt"
  }

  if [ $# -ne 3 ]; then
    echo "Wrong number of operands"
    usage
    return 128
  fi

  rc_is_less=1
  rc_is_equal=1
  rc_is_more=1
  case "$2" in
    \<|lt)
      rc_is_less=0
      ;;

    \<=|le)
      rc_is_less=0
      rc_is_equal=0
      ;;

    ==|=|eq)
      rc_is_equal=0
      ;;

    \>=|ge)
      rc_is_equal=0
      rc_is_more=0
      ;;

    \>|gt)
      rc_is_more=0
      ;;

    *)
      echo "Unknown operation: $2"
      usage
      return 128
      ;;
  esac

  if [[ $1 == $3 ]]; then
    # Trivial case, version numbers are equal
    return $rc_is_equal
  fi

  local IFS=.
  local i ver1=($1) ver2=($3)
  # fill empty fields in ver1 with zeros
  for ((i=${#ver1[@]}; i<${#ver2[@]}; i++)); do
    ver1[i]=0
  done

  for ((i=0; i<${#ver1[@]}; i++)); do
    if ((10#${ver1[i]:=0} > 10#${ver2[i]:=0})); then  # ver1 > ver2
      return $rc_is_more
    fi
    if ((10#${ver1[i]} < 10#${ver2[i]})); then  # ver1 < ver2
      return $rc_is_less
    fi
  done

  # ver1 = ver2
  return $rc_is_equal
}

__prompt_command() {
  # Get last return code
  local rc="$?"

  # Get current environnement
  local user=`whoami`
  local host=`hostname`
  local DEFAULT_USER="moamoak"

  # Initialize PS1
  PS1="\n"

  # Virtualenv
  if [[ -n $VIRTUAL_ENV ]]; then
    PS1+="\[$(right_align ${FG_RED} \($(basename $VIRTUAL_ENV)\))\]"
  fi

  # Username
  if [[ "$user" != "$DEFAULT_USER" ]]; then
    if [[ "$EUID" == 0 ]]; then
      PS1+="${NOSIZE_FG_RED}$user${NOSIZE_COLOR_RESET}"
    else
      PS1+="${NOSIZE_FG_GREEN}$user${NOSIZE_COLOR_RESET}"
    fi
  fi

  # Hostname
  if [[ -n "$container" ]]; then
    PS1+="@${NOSIZE_FG_YELLOW}lxc($host)${NOSIZE_COLOR_RESET}"
  elif [[ -n "$SSH_CONNECTION" ]]; then
    PS1+="@${NOSIZE_FG_YELLOW}$host${NOSIZE_COLOR_RESET}"
  fi

  # Separator
  PS1+="${NOSIZE_FG_BLUE}➤ ${NOSIZE_COLOR_RESET}"

  # Current directory
  if [[ -w "${PWD}" ]]; then
    PS1+="${NOSIZE_FG_GREEN}${PWD/#$HOME/\~}${NOSIZE_COLOR_RESET}"
  else
    PS1+="${NOSIZE_FG_RED}${PWD/#$HOME/\~}${NOSIZE_COLOR_RESET}"
  fi

  # Git infos
  local git_branch=`git branch 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
  if [[ -n $git_branch ]]; then
    local git_status_working_tree=`git status --porcelain 2>/dev/null | grep -c '^.[ACDMRTU?]'`
    local git_status_index=`git status --porcelain 2>/dev/null | grep -c '^[ACDMRTU]'`
    if [[ $git_status_working_tree != "0" || $git_status_index != "0" ]]; then
      local git_dirty="${NOSIZE_FG_MAGENTA}[${NOSIZE_FG_GREEN}$git_status_index${NOSIZE_FG_MAGENTA}|${NOSIZE_FG_RED}$git_status_working_tree${NOSIZE_FG_MAGENTA}]"
    else
      local git_dirty=""
    fi
    PS1+=" ${NOSIZE_FG_MAGENTA}<$git_branch$git_dirty>${NOSIZE_COLOR_RESET}"
  fi

  # Time and date
  PS1+=" ${NOSIZE_FG_YELLOW}[`date +%T`]${NOSIZE_COLOR_RESET}"

  # Return code
  if [[ "$rc" == 130 ]]; then
    PS1+=" ${NOSIZE_FG_YELLOW}(^C)${NOSIZE_COLOR_RESET}"
  elif [[ "$rc" != 0 ]]; then
    PS1+=" ${NOSIZE_FG_RED}($rc)${NOSIZE_COLOR_RESET}"
  fi

  # Newline
  PS1+="\n"
  
  # Prompt symbol
  if [[ "$EUID" == 0 ]]; then
    PS1+="${NOSIZE_FG_RED}#${NOSIZE_COLOR_RESET} "
  else
    PS1+="${NOSIZE_FG_GREEN}\$${NOSIZE_COLOR_RESET} "
  fi

  # Fix SSH agent forwarding for TMUX sessions
  if [[ -n $TMUX ]]; then
    tmux_fixsshagent
  fi

}

PROMPT_COMMAND=__prompt_command

export EDITOR='vim'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  fi
  if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
  if [ -f ~/.bash/bash_completion ]; then
    . ~/.bash/bash_completion
  fi
fi

## Include other files
if [ -d ~/.bash ]; then
for f in ~/.bash/*; do
    . $f
  done
fi
