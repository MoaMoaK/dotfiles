CUR_DIR=$(dirname $0)
BASH_RC=".bashrc"
BASH_DIR=".bash"

# link configuration
ln -fs "$CUR_DIR/$BASH_RC" "$HOME/$BASH_RC"
ln -fs "$CUR_DIR/$BASH_DIR" "$HOME/$BASH_DIR"
